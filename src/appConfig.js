export default function appConfig($locationProvider, $urlRouterProvider) {
  // specify url mode
  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise(($injector) => {
    const $state = $injector.get('$state');
    $state.go('home');
  });
}

appConfig.$inject = ['$locationProvider', '$urlRouterProvider'];
