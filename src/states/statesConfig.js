import homeStateConfig from './home/homeStateConfig'

export default function statesConfig($stateProvider) {
  homeStateConfig($stateProvider);
}

statesConfig.$inject = ['$stateProvider'];
