
export default function homeStateConfig($stateProvider) {
  $stateProvider.state('home', {
    url: '/home',
    controller: () => {},
    controllerAs: 'vm',
    template: '<sand-app></sand-app>',
  });
}
