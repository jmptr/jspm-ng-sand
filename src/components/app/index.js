import angular from 'angular'

class AppController {
  consturctor($log) {
    this.$log = $log;
    this.$log.info('AppController consturctor');
  }
  info(msg, ...opts) {
    this.$log.info(msg, ...opts);
  }
}

AppController.$inject = ['$log'];

const config = {
  bindings: {},
  controller: AppController,
  controllerAs: 'vm',
  template: '<span>Hi there</span>',
}

angular.component('sandApp', config);
