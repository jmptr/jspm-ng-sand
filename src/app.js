import angular from 'angular'
import appConfig from './appConfig';
import statesConfig from './states/statesConfig'
import 'angular-ui-router'

const moduleName = 'sandApp';
angular
  .module(moduleName, [
    'ui.router',
  ])
  .config(statesConfig)
  .config(appConfig);

/* eslint-disable */
// disable eslint error
// You should use the $document service instead of the default document object
angular.element(document).ready(() => {
  angular.bootstrap(document.body, [moduleName], { strictDi: true });
  document.body.className = `${document.body.className} ng-app`;
});
/* eslint-enable */

export default angular.module(moduleName);
